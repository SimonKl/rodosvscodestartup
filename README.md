# RodosVSCodeStartup

This is a simple repo that shows how a Rodos Project using VSCode is setup.

## Download
In your prefered directory: 
```
git clone https://gitlab.com/SimonKl/rodosvscodestartup.git --recursive
```
This downloads this repo along with the rodos version that was current during its creation. To update rodos, do:

```
cd rodos
git pull origin master
```

## Building
This uses CMake as the build System. The build folder is `build`. You can launch the build task using `Ctrl-Shift-B`, it is confugred in `.vscode/tasks.json` and equivalent to:
```shell
mkdir -p build && cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../rodos/cmake/port/linux-x86.cmake -DCMAKE_BUILD_TYPE=Debug -DEXECUTABLE=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
make -j my-application
```

You can add more tasks to this file to define different build processes (change `isDefault` to false respectively). What gets build is defined in `CMakeLists.txt` files. The top-level one must include rodos, so it exposes the command `add_rodos_executable` to add `.cpp` to your compilation.

Here an theoratical example using multiple source files:
```CMakeLists.txt
add_subdirectory(rodos)
add_rodos_executable(my-application
        basic.cpp
        another_file.cpp
        even_more_files.cpp
)
```

The first argument of `add_rodos_executable` is the name of the executable. By adding multiple calls to `add_rodos_executable` with different executable names you can build several applications at once.

You can define include-directories like this: 
```CMakeLists.txt
add_subdirectory(rodos)
add_rodos_executable(my-application basic.cpp)
target_include_directories(my-application myincludedir)
```

All include-dirs Rodos needs are already defined in its own `CMakeLists.txt` files.

To change which platform you want to build for, in the task, change `-DCMAKE_TOOLCHAIN_FILE` variable. The default is linux. You can see all avaicable platforms in `rodos/cmake/port/`. You may need to change `-DCMAKE_BUILD_TYPE` from `Debug` to `Release`.


## Launching / Debugging
When building for Linux/Posix with this setup it is fairly easy to have Debugging. For it to work properly, make sure that `-DCMAKE_BUILD_TYPE=Debug` is set.

In `.vscode/launch.json` a launch profile is set. It just runs the previously described build task, and then launches the build application with an attached debugger. You can of course add new launch profiles to this file. To use it, you can go to the Debug-Tab of VSCode:

![tabpanelpic](docImages/tabPanel.png)

Press the green Play button, or `F5`, to run the app in the debugger. You can add Breakpoints to lines by clicking just left of the line number. (Note that Breaking is going to severly mess with the timings of the scheduler).
In the same panel you will then be able to see all variables that are currently in scope. For structs and such, you can click on them to expand them to see the individual values.